/** @type IPreferences */
export const preferences = {
  debug: process.env.NODE_ENV === 'production',
  nightMode: false,
};


/** @type IGameState */
export const game = {
  type: 'wander',

  history: [],
  startFrom: 'Foyer',
  passage: null,
  chapter: null,

  validPassages: [],

  goTo(destination) {
    if (this.validPassages.includes(destination)) {
      this.history.push(destination);
      this.passage = destination;
    } else {
      const msg = `No such passage: ${destination}`;
      if (preferences.debug) {
        alert(msg);
      } else {
        console.error(msg);
      }
    }
  },

  goBack() {
    if (this.type === 'unfold') {
      this.history.pop();
      this.passage = this.history[this.history.length - 1];
    } else {
      const length = this.history.length;
      const index = length <= 2 ? 0 : length - 2;
      this.goTo(this.history[index]);
    }
  },

  theme: 'vampire',

  displayGameName: true,
  displayChapterName: true,
  displayPassageName: true,

  showError(error) {
    if (preferences.debug) {
      alert(error);
    } else {
      console.error(error);
    }
  },
};


/** @type IPlayer */
export const player = {
  name: 'Player',
  hasCloak: true,
  hasStumbled: false,
};
