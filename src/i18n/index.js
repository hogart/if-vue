import MultiLanguage from 'vue-multilanguage';

import en from './en.json';
import ru from './ru.json';

export function install(Vue) {
  Vue.use(MultiLanguage, {
    default: 'en',

    en,
    ru,
  });
}
