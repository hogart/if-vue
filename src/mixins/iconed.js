import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
import '@fortawesome/fontawesome/styles.css';

fontawesome.config.autoAddCss = false;

export const iconed = {
  components: {
    FontAwesomeIcon,
  },
};
