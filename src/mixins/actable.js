import {onceable} from './onceable';
import {disableable} from './disableable';

export const actable = {
  mixins: [onceable, disableable],

  computed: {
    inactive() {
      if (this.disabled) {
        return true;
      } else {
        return this.once && !this.active;
      }
    },
  },
};
