export const disableable = {
  props: {
    disabled: {
      type: Boolean,
      default: undefined,
    },
  },

  computed: {
    inactive() {
      return this.disabled;
    },
  },
};
