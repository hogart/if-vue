export const named = {
  props: {
    name: {
      type: String,
      required: true,
    },
  },
};
