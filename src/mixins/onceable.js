export const onceable = {
  props: {
    once: {
      type: Boolean,
      default: undefined,
    },
  },

  data() {
    return {
      active: true,
    };
  },

  computed: {
    inactive() {
      return this.once && !this.active;
    },
  },
};
