export const IvGameState = {
  install(Vue) {
    Vue.mixin({
      store: ['game', 'player', 'preferences'],
    });
  },
};
