import IvAction from '../IvAction';
import IvChapter from '../IvChapter';
import IvCheckbox from '../IvCheckbox';
import IvChooser from '../IvChooser';
import IvChosen from '../IvChosen';
import IvDelayed from '../IvDelayed';
import IvExpand from '../IvExpand';
import IvLink from '../IvLink';
import IvNote from '../IvNote';
import IvPassage from '../IvPassage';
import IvPreferences from '../IvPreferences';
import IvStory from '../IvStory';

export const IvComponents = {
  install(Vue) {
    Vue.component(IvAction.name, IvAction);
    Vue.component(IvChapter.name, IvChapter);
    Vue.component(IvCheckbox.name, IvCheckbox);
    Vue.component(IvChooser.name, IvChooser);
    Vue.component(IvChosen.name, IvChosen);
    Vue.component(IvDelayed.name, IvDelayed);
    Vue.component(IvExpand.name, IvExpand);
    Vue.component(IvLink.name, IvLink);
    Vue.component(IvNote.name, IvNote);
    Vue.component(IvPassage.name, IvPassage);
    Vue.component(IvPreferences.name, IvPreferences);
    Vue.component(IvStory.name, IvStory);
  },
};
