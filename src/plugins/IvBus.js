import Vue from 'vue';

const bus = new Vue();

export const IvBus = {
  install(Vue) {
    Object.defineProperties(Vue.prototype, {
      $bus: {
        get() {
          return bus;
        },
      },
    });
  },
};
