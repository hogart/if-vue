function tagToData(tag) {
  const uppercased = tag.substr(0, 1).toLocaleUpperCase() + tag.substr(1);
  return `tag${uppercased}`;
}

export const ivTags = {
  update(el, {value, oldValue}) {
    if (oldValue && oldValue.length) {
      oldValue.forEach((tag) => delete el.dataset[tagToData(tag)]);
    }

    if (value && value.length) {
      value.forEach((tag) => el.dataset[tagToData(tag)] = '');
    }
  },
};
