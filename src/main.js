import Vue from 'vue';
import VueStash from 'vue-stash';
import Meta from 'vue-meta';

import {IvBus} from './plugins/IvBus';
import {IvGameState} from './plugins/IvGameState';
import {IvComponents} from './plugins/IvComponents';
import {install as installI18n} from './i18n/index';

import CloakOfDarkness from './CloakOfDarkness.vue';
import { game, player, preferences } from './gameState';

Vue.use(VueStash);
Vue.use(Meta);
installI18n(Vue);

Vue.use(IvBus);
Vue.use(IvGameState);
Vue.use(IvComponents);

new Vue({
  el: '#app',

  data: {
    store: {
      game,
      player,
      preferences,
    },
  },

  render: h => h(CloakOfDarkness),
});
