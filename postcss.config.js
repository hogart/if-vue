'use strict';

function postCssConfig(ctx) {
  return {
    map: true,
    plugins: {
      autoprefixer: {},
      cssnano: ctx.env === 'production' ? false : {},
    },
  };
}

module.exports = postCssConfig;
