'use strict';

const path = require('path');
const webpack = require('webpack');
const eslintUnixFormatter = require('eslint/lib/formatters/unix');

const isProduction = process.env.NODE_ENV === 'production';

const config = {
  entry: './src/main.js',

  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'game.js',
  },

  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        include: path.resolve(__dirname, 'src'),
        loader: 'eslint-loader',
        options: {
          quiet: true, // yelling about warnings is messing up console output
          formatter: eslintUnixFormatter,
          cache: true,
        },
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // other vue-loader options go here
          },
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        },
      },
    ],
  },

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
    },
    extensions: ['*', '.js', '.vue'],
  },

  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true,
  },

  performance: {
    hints: false,
  },

  devtool: isProduction ? '#source-map' : '#eval-source-map',

  plugins: [],
};

if (isProduction) {
  // http://vue-loader.vuejs.org/en/workflow/production.html
  config.plugins = config.plugins.concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false,
      },
    }),

    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
  ]);
}

module.exports = config;
