export type TGameType = 'wander' | 'unfold';

/** Game state: passages history and history utility methods. */
export interface IGameState {
  /** 'unfold' for interactive novels, where text is appended, 'wander' for more free-form experience */
  type: TGameType;

  /** Visited passages names */
  history: string[];
  /** Starting passage name */
  startFrom: string;
  /** Current passage */
  passage: string;

  /** All existing passages names */
  validPassages: string[],

  /** Go to given passage if exists (if not, errors message will be shown in browser or console) */
  goTo: (passage: string) => void;

  /** Go back in history. Hides visited passages in unfold mode */
  goBack: () => void;

  /** Current visual theme */
  theme: string;

  /** Current chapter title, if any */
  chapter: string;

  displayGameName: boolean;
  displayChapterName: boolean;
  displayPassageName: boolean;

  showError: (error: string) => void;
}

/** UI preferences: debug, day/night skin, sound volume, etc.*/
export interface IPreferences {
  debug: boolean;
  nightMode?: boolean;
  soundMuted?: boolean;
}

/** Player stats, attributes, choices go here */
export interface IPlayer {

}
