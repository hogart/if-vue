'use strict';

const eslintConfig = {
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    }
  },
  plugins: ['eslint-plugin-vue'],
  env: {
    browser: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended'
  ],
  rules: {
    semi: ['error', 'always'],
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'ignore',
    }],
    'no-console': ['warn'],
    'indent': ['error', 2],
    'no-debugger': [process.env.NODE_ENV === 'production' ? 'error' : 'warn'],
    quotes: ['error', 'single', {allowTemplateLiterals: true}],
  },
};

module.exports = eslintConfig;
