# IV

IV (pronounced same as "ivy") is interactive fiction library/framework written with Vue.js. It's idea and capabilities are inspired by [Windrift](https://github.com/lizadaly/windrift) and [SugarCube](http://www.motoslave.net/sugarcube/2/).

## Components

## `iv-story`

Contains your story. `iv-chapter`'s and `iv-passage`'s go inside this tag.

```html
<iv-story lang="es" :title-template="() => `Mi nombre es ${this.player.name || 'Ishmael'}`"></iv-story>
```

`lang` is optional and defaults to `'en'`, but it's useful for screen readers and browsers which support hyphenation.

`title-template` is optional string or function which changes document title. It defaults to `${currentPassage.title}  —  ${story.title}`.

## `iv-chapter`

You can group your passages in chapters. This is useful, for example, to change visual theme (or anything else) based on player's location in game world. Besides, chapter name can be displayed at the top of the screen.

```html
<iv-chapter title="In the dungeon" theme="overcast"></iv-story>
```

If you decide to have chapters, you need to put every passage into some chapter, or game will behave unpredictably, especially in unfold mode.

### `iv-passage`

Basic building block. Represents location or description or anything.

```html
<iv-passage name="Just any passage title" tags="tag another-tag">
  <p>You (your name is {{ player.name }}) suddenly wake up in the middle of the night. Somethings's <iv-link to="Second">off</iv-link>.</p>
</iv-passage>

```

To make passage starting, give it special name:

```html
<iv-passage :name="game.startFrom"></iv-passage>
```

#### Events:

* `onEnter` when player visits this passage
* `onLeave` when player leaves to another passage (fires before `onEnter` for target passage)

### `iv-link`

Thing that player clicks to go to another passage.
```html
<iv-link to="Basement">Go to basement</iv-link>
```

Sometimes you want to direct player back to whence they came from without knowing exact location. Use `to="::back"` or empty `back` attribute:
```html
<iv-link back>Close the door and slowly back up</iv-link>

<iv-link to="::back">Return</iv-link>
```

`iv-link` can be disabled programmatically using `disabled` attribute, in this case it will look like plain text and will not perform transition. This allows you to, for example, hide passage exits from the player until they examine something or push some buttons.

#### Events:

* `onAct` when clicked or tapped (fires before `onLeave` on containing passage)

### `iv-expand`

Thing that looks like link, but instead of navigating between passages it appends, prepends or replaces it's own text.

Default behavior (no `type` attribute) is to append:
```html
I'd prefer <iv-expand text="ice-cream">, you know</iv-link>.
```
After clicking highlighted "ice-cream" word, becomes "I'd prefer ice-cream, you know."

```html
I'd prefer <iv-expand text="ice-cream" type="replace"> ... actually no, I'd rather go for some apple pie</iv-link>.
```
After clicking highlighted "ice-cream" word, becomes "I'd prefer ... actually no, I'd rather go for some apple pie."

```html
I'd prefer <iv-expand text="strawberry" type="prepend">ice-cream</iv-link>.
```
After clicking highlighted "ice-cream" word, becomes "I'd prefer strawberry ice-cream."

You can nest them.

`iv-expand` can be disabled programmatically, in this case it will look like plain text and will not emit event on click.

#### Events:

* `onAct` when clicked or tapped

### `iv-action`

Generic cousin of `iv-expand` and `iv-link`, clickable thing without predefined behavior.

```html
<iv-action @onAct="() => alert('Gardener!')">Click here to reveal murderer</iv-action>
```

With `once` attribute, `iv-action` will turn into plain text after first click.

```html
<iv-action @onAct="() => alert('Missile launched! No missiles left')" once>Launch missile</iv-action>
```

As `iv-link` and `iv-expand`, `iv-action` can be programmatically disabled, which allows you to hide available actions from player until they performs some steps first.

```html
<iv-action @onAct="() => player.nuclearBriefcase = true">Unlock nuclear briefcase</iv-action>
<iv-action @onAct="() => alert('Missile launched! No missiles left')" disabled="!player.nuclearBriefcase">Launch missile</iv-action>
```

#### Events:

* `onAct` when clicked or tapped

## `iv-note`

Simple thing to display side-notes or some such on some text. Basically it works like `title=""` attribute, but is accessible on touch devices too (by tap).
```html
Dungeon walls are dripping with <iv-note text="you accidentally touch it and decide not to drink it">water</iv-note>.
```

#### Events:

* `onAct` hovered with mouse or tapped

## `iv-delayed`

`iv-delayed` reveals content automatically after given delay (in microseconds, so 1 second is 1000).
```html
<p>This is great idea!</p>
<iv-delayed after="1000" t8n>On second thought, maybe it's not.</iv-delayed>.
```

`delay` is counted from the moment component is attached to actual DOM.

`t8n` attribute makes content appear from transparency with transition.

#### Events:

* `onAct` fires when shows it's content

## `iv-chooser` and `iv-chosen`

`iv-chooser` looks like pseudo-link, which when clicked sets defined values into player state. Probably the most common use-case would be defining player character's gender, hair and eye color and so on.

`iv-chosen` is utility component which simplifies displaying content based on choices made by `iv-choser`.
```html
<p>I looked in the mirror, at my <iv-chooser ckey="eyeColor" :values="{green: 'emerald', brown: 'chestnut', blue: 'sapphire', gray: 'steelish'}" :loop="false"></iv-chooser> eyes. My step-sister chuckled behind me: <br/>
"You sure stare in the mirror a lot. Your <iv-chosen ckey="eyeColor" :values="{green: 'swampy', brown: 'tea-colored', blue: 'watered', gray: 'dull'}"></iv-chosen> eyes are not that beautiful!
</p>
```
In this example `loop` is set to false which means choice is disabled once player reaches last option.

Don't rely on order of `values`: in most browsers choices will be cycled in order they are listed in object, but in some they may not.

`iv-chooser` can be disabled programmatically using `disabled` attribute, in this case it will look like plain text and will not change variable value when clicked.

#### Events:

* `onAct` fires when `iv-choser` is clicked or tapped



## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
